const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const { User_Game } = require('../models');
const bcrypt = require('bcrypt');

async function authenticate(email, password, done) {
    try {
        const user = await User_Game.findOne({ where: { email }});
        if (!user) {
            return done(null, false, {
                message: 'user not found'
            });
        }

        const  valid = await bcrypt.compare(password, user.password);
        if (!valid) {
            return done(null, false, {
                message: 'invalid password'
            });
        }

        return done(null, user);
    } catch (err) {
        return done(null, false, {
            message: err.message
        });
    }
}

passport.use(new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, authenticate))

passport.serializeUser((user, done) => {
    return done(null, user.id);
})

passport.deserializeUser(async (id, done) => {
    return done(null, await User.findOne({ where: { id }}));
})

module.exports = passport;