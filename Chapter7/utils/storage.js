const multer = require('multer');
const path  = require('path');

const simpan = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './upload/media');
    },

    filename: (req, file, callback) => {
        const fileNama = Date.now() + '_' + file.originalname;
        callback(null, fileNama);
    }
})

module.exports = {
    image: multer({
        storage: simpan,
        limits: {
            fileSize: 1024 * 1024 * 1
        },

        fileFilter: (req, file, callback) => {
            if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
                callback(null, true)
            
            } else {
                const err = new Error('hanya file bertipe png, jpg, dan jpeg yang diizinkan');
                callback(err, false);
            }

        },

        onError: (err, next) => {
            next(err);
        }
    }),

    video: multer({
        storage: simpan,
        limits: {
            fileSize: 10240 * 1024 * 1
        },

        fileFilter: (req, file, callback) => {
            if (file.mimetype == 'video/mp4' || file.mimetype == 'video/3gp' || file.mimetype == 'video/avi') {
                callback(null, true)
            } else {
                const err = new Error('hanya video bertipe mp4, 3gp, dan avi yang diizinkan');
                callback(err, false);
            }
        },

        onError: (err, next) => {
            next(err);
        }
    })
}