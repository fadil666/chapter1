const { User_Game, User_Biodata, User_History, Image, Video } = require('../models');
const { Op } = require("sequelize");
const bcrypt = require('bcrypt');
const roles = require('../utils/roles')
const usertypes = require('../utils/usertype');




const {
    JWT_TOKEN,
} = process.env;

module.exports = {
    daftarAdmin: async (req, res, next) => {
        
        try {
            const { username, email, password, role = roles.Admin, usertype = usertypes.Basic } = req.body;

            const sudahAda = await User_Game.findOne({ where: { email: email } });
            if (sudahAda) {
                return res.status(409).json({
                    status: false,
                    message: 'email sudah terpakai!!!'
                    
                })
            }

            const encr = await bcrypt.hash(password, 10);
            const regis = await User_Game.create({
                username,
                email,
                password: encr,
                role,
                usertype
            })

            return res.status(201).json({
                status: true,
                message: 'akun berhasil dibuat',
                data: {
                    username: regis.username,
                    email: regis.email,
                    password: regis.password,
                    role: regis.role,
                    usertype: regis.usertype
                }
            })

        }catch (err) {
            next(err);
        }
    },

    masukAdmin: async (req, res, next) => {
        try {
            const user = await User_Game.authenticate(req.body);
            const accesstoken = user.generateToken();

            const date = new Date();
            const formatDate = date.toLocaleDateString('id');
            var curr_hour = date.getHours();
		    var curr_minute = date.getMinutes();
		    var curr_second = date.getSeconds();

            const jam = curr_hour + ':' + curr_minute + ':' + curr_second;

            res.status(200).json({
                status: true,
                message: 'success login',
                data: {
                    id: user.id,
                    name: user.username,
                    email: user.email,
                    role: user.role,
                    usertype: user.usertype,
                    date: formatDate,
                    time: jam,
                    access_token: accesstoken
                }
            });
        } catch (err) {
            next(err);
        }
    },

    akunAdmin: (req, res, next) => {
        const user = req.user;
        
        try {
            return res.status(200).json({
                status: true,
                message: 'autentifikasi berhasil',
                data: {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    role: user.role,
                    usertype: user.usertype
                }
            });
            
        }catch (err) {
            next(err);
        }
        
    },

    gantiPasswordAdmin: async (req, res, next) => {

        try {
            const { 
                 passwordLama,
                 passwordBaru,
                 passwordBaru2
            } = req.body;

            const usercompare = await User_Game.findOne({ 
                where: { 
                    id: req.user.id
                }});
            if (!usercompare) {
                return res.status(400).json({
                    status: false,
                    message: 'user tidak di temukan!'
                })
            }
    
            const pass = await bcrypt.compare(passwordLama, usercompare.password);
            if (!pass) {
                return res.status(400).json({
                    status: false,
                    message: 'password salah!!'
                })
            }

            if (passwordBaru !== passwordBaru2) 
            return res.status(422).json({
                status: false,
                message: 'password 1 dan password 2 tidak sama!'
            });

            const hashedPassword = await bcrypt.hash(passwordBaru, 10);
             await usercompare.update({password: hashedPassword});

            return res.status(200).json({
                success: true,
                message: 'Password berhasil di ubah'
            });
        } catch (err) {
            res.status(500).json({
                status: false, 
                message: err.message
            });
        }
    },

    hapusAdmin: async (req, res, next) => {
        try {
        const { userId } = req.params;
        // ini akan menghapus user, histori game, dan biodata berdasarkan id
        const user = await User_Game.findOne({ 
            where: { 
                [Op.and]: [
                    { id: userId },
                    { role: roles.Admin }
                  ] 
            } 
        });
            if (!user) return res.status(404).json({ 
                status: false, 
                message: `User dengan id ${userId} tidak di temukan!` 
            });
        
            await User_Game.destroy({ where: { id: userId }});

        return res.status(200).json({
            status: true,
            message: 'data user telah berhasil di hapus',
            data: user
        })
    } catch (err) {
        next(err);
    }

    },

    daftarAdminAll: async  (req, res, next) => {
        
        try { 
           const users = await User_Game.findAll({ where: {
            role: roles.Admin
           }});

        return res.status(200).json({
            status: true,
            message: 'berhasil dapat data',
            data: users
        })
    }catch (err) {
        next(err);
    }  
    },

    detilAdmin: async (req, res, next) => {
        try {
            const { userId } = req.params;

            const user = await User_Game.findOne({ 
                where: { 
                    [Op.and]: [
                        { id: userId },
                        { role: roles.Admin }
                      ]
                    }
            });
                
                if (!user) return res.status(404).json({ 
                    status: false, 
                    message: `User dengan id ${userId} tidak ditemukan` 
                });
    
            return res.status(200).json({
                status: true,
                message: 'data berhasil ditemukan',
                data: user
            })
        } catch (err) {
            next(err);
        }
    },

    upload: async (req, res, next) => {
        try{
            const { nama_file, url_file } = req.body;
            const user = req.user;

            const media = await Image.create({
                admin_id: user.id,
                nama_file,
                url_file
                
            })

            return res.status(200).json({
                status: true,
                message: 'file uploaded!',
                data: media
            });


        }catch (err){
            next(err);
        }
    },

    uploadVideo: async (req, res, next) => {
        try{
            const { nama_file, url_file } = req.body;
            const user = req.user;

            const media = await Video.create({
                admin_id: user.id,
                nama_file,
                url_file
                
            })

            return res.status(200).json({
                status: true,
                message: 'file uploaded!',
                data: media
            });


        }catch (err){
            next(err);
        }
    }
}