const { User_Game, User_Biodata, User_History, Image, Video } = require('../models');
const { Op } = require("sequelize");
const bcrypt = require('bcrypt');
const jsonToken = require('jsonwebtoken');
const roles = require('../utils/roles')
const usertypes = require('../utils/usertype');
const googleoauth = require('../utils/oauthgoogle');
const facebookoauth = require('../utils/oauthfacebook');

const {
    JWT_TOKEN
} = process.env;

module.exports = {
    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            if (!code) {
                const url = googleoauth.generateAuthUrl();
                return res.redirect(url);
            }

            await googleoauth.setCredentials(code);

            const { data } = await googleoauth.getDataUser();

            let exist = await User_Game.findOne({
                where: {
                    email: data.email
                }
            })

            if (!exist) {
                exist = await User_Game.create({
                    username: data.name,
                    email: data.email,
                    role: roles.User,
                    usertype: usertypes.Google
                })
            }

            const payload = {
                id: exist.id,
                username: exist.username,
                email: exist.email,
                role: exist.role,
                usertype: exist.usertype
            }

            const token1 = jsonToken.sign(payload, JWT_TOKEN);

            return res.status(200).json({
                status: true,
                message: 'berhasil',
                data: {
                    id: exist.id,
                    username: exist.username,
                    email: exist.email,
                    token: token1,
                    code: code
                }
            })
        }catch (err){
            next(err);
        }
    },

    facebook: async (req, res, next) => {
        try {
        const code = req.query.code;

            if (!code) {
                const url = facebookoauth.generateAuthURL();
                return res.redirect(url);
            }

            const access_token = await facebookoauth.getAccessToken(code);

            const userInfo = await facebookoauth.getUserInfo(access_token);

            let userExist = await User_Game.findOne({ where: { email: userInfo.email } });

            if (!userExist) {
                userExist = await User_Game.create({
                    username: [userInfo.first_name, userInfo.last_name].join(' '),
                    email: userInfo.email,
                    role: roles.User,
                    usertype: usertypes.Facebook
                });
            }

            const payload = {
                id: userExist.id,
                username: userExist.username,
                email: userExist.email,
                role: userExist.role,
                usertype: userExist.usertype
            };
            const token = jsonToken.sign(payload, JWT_TOKEN);

            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    id: userExist.id,
                    username: userExist.username,
                    email: userExist.email,
                    role: userExist.role,
                    usertype: userExist.usertype,
                    token: token,
                    code: access_token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    daftar: async (req, res, next) => {
        
        try {
            const { username, email, password, role = roles.User, usertype = usertypes.Basic } = req.body;

            const sudahAda = await User_Game.findOne({ where: { email: email } });
            if (sudahAda) {
                return res.status(409).json({
                    status: false,
                    message: 'email sudah terpakai!!!'
                    
                })
            }

            const encr = await bcrypt.hash(password, 10);
            const regis = await User_Game.create({
                username,
                email,
                password: encr,
                role,
                usertype
            })

            return res.status(201).json({
                status: true,
                message: 'akun berhasil dibuat',
                data: {
                    username: regis.username,
                    email: regis.email,
                    password: regis.password,
                    role: regis.role,
                    usertype: regis.usertype
                }
            })

        }catch (err) {
            next(err);
        }
    },

    masuk: async (req, res, next) => {
        try {
            const user = await User_Game.authenticate(req.body);
            const accesstoken = user.generateToken();

            res.status(200).json({
                status: true,
                message: 'success login',
                data: {
                    id: user.id,
                    name: user.username,
                    email: user.email,
                    role: user.role,
                    usertype: user.usertype,
                    access_token: accesstoken,
                }
            });
        } catch (err) {
            next(err);
        }
    },

    akunSaya: (req, res, next) => {
        const user = req.user;
        
        try {
            const date = new Date();
            const formatDate = date.toLocaleDateString('id');
            var curr_hour = date.getHours();
		    var curr_minute = date.getMinutes();
		    var curr_second = date.getSeconds();

            const jam = curr_hour + ':' + curr_minute + ':' + curr_second;

            return res.status(200).json({
                status: true,
                message: 'autentifikasi berhasil',
                data: {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    role: user.role,
                    usertype: user.usertype,
                    date: formatDate,
                    time: jam
                }
            });
            
        }catch (err) {
            next(err);
        }
        
    },


    gantiPassword: async (req, res, next) => {

        try {
            const { 
                 passwordLama,
                 passwordBaru,
                 passwordBaru2
            } = req.body;

            const usercompare = await User_Game.findOne({ 
                where: { 
                    id: req.user.id
                }});
            if (!usercompare) {
                return res.status(400).json({
                    status: false,
                    message: 'user tidak di temukan!'
                })
            }
    
            const pass = await bcrypt.compare(passwordLama, usercompare.password);
            if (!pass) {
                return res.status(400).json({
                    status: false,
                    message: 'password salah!!'
                })
            }

            if (passwordBaru !== passwordBaru2) 
            return res.status(422).json({
                status: false,
                message: 'password 1 dan password 2 tidak sama!'
            });

            const hashedPassword = await bcrypt.hash(passwordBaru, 10);
             await usercompare.update({password: hashedPassword});

            return res.status(200).json({
                success: true,
                message: 'Password berhasil di ubah'
            });
        } catch (err) {
            res.status(500).json({
                status: false, 
                message: err.message
            });
        }
    },

    hapususer: async (req, res, next) => {
        try {
        const { userId } = req.params;
        // ini akan menghapus user, histori game, dan biodata berdasarkan id
        const user = await User_Game.findOne({ 
            where: { 
                [Op.and]: [
                    { id: userId },
                    { role: roles.User }
                  ]
            } 
        });
            if (!user) return res.status(404).json({ 
                status: false, 
                message: `User dengan id ${userId} tidak di temukan!` 
            });
        
            await User_Game.destroy({ where: { id: userId }});
            await User_Biodata.destroy({where: { id_user : userId}});
            await User_History.destroy({where: { id_user : userId}});

        return res.status(200).json({
            status: true,
            message: 'data user telah berhasil di hapus',
            data: user
        })
    } catch (err) {
        next(err);
    }

    },

    daftarUser: async  (req, res, next) => {
        
        try { 
           const users = await User_Game.findAll({
            where: {
                role: roles.User
            }
           });

        return res.status(200).json({
            status: true,
            message: 'berhasil dapat data',
            data: users
        })
    }catch (err) {
        next(err);
    }  
    },

    detilUser: async (req, res, next) => {
        try {
            const { userId } = req.params;

            const user = await User_Game.findOne({ 
                where: { 
                    [Op.and]: [
                        { id: userId },
                        { role: roles.User }
                      ]
                    }
            });
                
                if (!user) return res.status(404).json({ 
                    status: false, 
                    message: `User dengan id ${userId} tidak ditemukan` 
                });
    
            return res.status(200).json({
                status: true,
                message: 'data berhasil ditemukan',
                data: user
            })
        } catch (err) {
            next(err);
        }
    },

    getAllImages: async (req, res, next) => {
        try{

            const images = await Image.findAll();

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: images
            })

        }catch (err){
            next(err);
        }
    },

    getAllVideos: async (req, res, next) => {
        try{

            const videos = await Video.findAll();

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: videos
            })

        }catch (err){
            next(err);
        }
    },

    getImages: async (req, res, next) => {
        try{
            const { imageId } = req.params;
            const images = await Image.findOne({
                where: {
                    id: imageId
                }
            });

            if (!images) {
                return res.status(404).json({
                    status: false,
                    message: `gambar dengan ${imageId} tidak ditemukan`,
                    data: null
                })
            }

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: images
            })

        }catch (err){
            next(err);
        }
    },

    getVideos: async (req, res, next) => {
        try{
            const { videoId } = req.params;
            const videos = await Video.findOne({
                where: {
                    id: videoId
                }
            });

            if (!videos) {
                return res.status(404).json({
                    status: false,
                    message: `video dengan ${videoId} tidak ditemukan`,
                    data: null
                })
            }

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: videos
            })

        }catch (err){
            next(err);
        }
    },
}