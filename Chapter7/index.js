const express = require('express');
const morgan = require('morgan');
const router = require('./routes/index');
require('dotenv').config();
const index = express();
const path = require('path');
// const passport = require('passport');
const cookieParser = require('cookie-parser');
// const session = require('express-session');
// const flash = require('express-flash');

index.use(express.json());
index.use(morgan('dev'));
// index.use(session({
//     secret: process.env.JWT_TOKEN,
//     resave: false,
//     saveUninitialized: false
// }))
// index.use(passport.initialize());
// index.use(passport.session());
// index.use(flash());

index.set('view engine', 'ejs');

index.use(router);
index.use('/images', express.static('upload/media'))
index.use('/videos', express.static('upload/media'))
index.use(express.urlencoded({ extended: false }));
index.use(express.static(path.join(__dirname, 'public')));
index.use(cookieParser());

index.use('/', router);

index.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'hayo tersesat ya?'
    })
})

index.use((err, req, res, next) => {
    if(err.code == 'LIMIT_FILE_SIZE' || err.message == 'file too large'){
        return res.status(500).json({
            status: false,
            message: "ukuran file terlalu besar maksimal 1 MB untuk images dan untuk video maksimal 10 MB"
        })
    } else {
    return res.status(500).json({
        status: false,
        message: err.message
    })
}
})

module.exports = index;