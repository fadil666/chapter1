'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Image extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Image.init({
    admin_id: DataTypes.INTEGER,
    nama_file: DataTypes.STRING,
    url_file: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Image',
  });
  return Image;
};