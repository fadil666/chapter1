// async function notifPermission() {
//     return new Promise(async (resolve) => {
//         resolve(await Notification.requestPermission());
//     });
// }


// // function isServiceWorkerSupported() {
// //     if ('serviceWorker' in navigator) return true;


// //     return false;
// // }

const VAPID_PUBLIC_KEY1 = 'BIFAtJLt1L77lrA8yuK409HG-sSImD9NZl9gVA7gX7SFYBZAIUx20M4qXvWnnSiK8OOgAWYU3GNMlaFOSVzfMyc'

async function registerServiceWorker() {
    const register = await navigator.serviceWorker.register('./worker.js', {
        scope: '/'
    })

    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: VAPID_PUBLIC_KEY1
    })

    await fetch('/notification/subscribe', {
        method: 'POST',
        body: JSON.stringify(subscription),
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

registerServiceWorker();
