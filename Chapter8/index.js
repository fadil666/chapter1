require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const router = require('./routes/index');
const index = express();
const methodOverride = require('method-override');
const cors = require('cors');
const webpush = require('web-push');
const path = require('path')
const fs = require('fs')
// const passport = require('passport');
const cookieParser = require('cookie-parser');
// const session = require('express-session');
// const flash = require('express-flash');

const {
    VAPID_PUBLIC_KEY,
    VAPID_PRIVATE_KEY,
    VAPID_SUBJECT
} = process.env;


// index.use(session({
    //     secret: process.env.JWT_TOKEN,
    //     resave: false,
    //     saveUninitialized: false
    // }))
    // index.use(passport.initialize());
    // index.use(passport.session());
    // index.use(flash());
    
index.use(express.urlencoded({ extended: true }));
index.use(morgan('dev'));
index.use(methodOverride('_method'));
index.set('view engine', 'ejs');

index.use(router);
index.use('/images', express.static('upload/media'))
index.use('/videos', express.static('upload/media'))
index.use(express.urlencoded({ extended: false }));
index.use(express.static(path.join(__dirname, 'public')));
index.use(cookieParser());

index.use(cors());
index.use(express.json());
index.use(express.static(path.join(__dirname, 'client')));

index.use(router);

webpush.setVapidDetails(VAPID_SUBJECT, VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

let sub = require('./subscriptions.json');
console.log(sub.length);
sub.splice(1, sub.length);

index.post('/notification/subscribe', async (req, res) => {
    try {
        const subscriptions = require('./subscriptions.json');
        const subscription = req.body;
        console.log(subscription);

        subscriptions.push(subscription);
        fs.writeFile('./subscriptions.json', JSON.stringify(subscriptions), (err) => {
            console.log = (err);
        });


        const payload = JSON.stringify({
            title: 'halo, selamat datang di website kami',
            body: 'silahkan login'
        })


        const result = await webpush.sendNotification(subscription, payload)

        return res.status(200).json({
            status: true,
            message: '',
            data: result
        })
    }catch (err) {
        return res.status(500).json({
            status: false,
            message: err.message
        })
    }
})

index.get('/auth/daftar', (req, res) => {
    const subscriptions = require('./subscriptions.json');

    const payload = JSON.stringify({
        title: '',
        body: '',
    });

    subscriptions.forEach(subscription => {
        webpush.sendNotification(subscription, payload)
            .then(result => console.log(result))
            .catch(e => console.log(e.stack));
    });

    res.status(200).json({ 'success': true });
});

index.get('/user/pesan', (req, res) => {
    const subscriptions = require('./subscriptions.json');

    const payload = JSON.stringify({
        title: '',
        body: '',
    });

    subscriptions.forEach(subscription => {
        webpush.sendNotification(subscription, payload)
            .then(result => console.log(result))
            .catch(e => console.log(e.stack));
    });

    res.status(200).json({ 'success': true });
});

index.get('/user/reset-password', (req, res) => {
    const subscriptions = require('./subscriptions.json');

    const payload = JSON.stringify({
        title: '',
        body: '',
    });

    subscriptions.forEach(subscription => {
        webpush.sendNotification(subscription, payload)
            .then(result => console.log(result))
            .catch(e => console.log(e.stack));
    });

    res.status(200).json({ 'success': true });
});

index.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'hayo tersesat ya?'
    })
})

index.use((err, req, res, next) => {
    if(err.code == 'LIMIT_FILE_SIZE' || err.message == 'file too large'){
        return res.status(500).json({
            status: false,
            message: "ukuran file terlalu besar maksimal 1 MB untuk images dan untuk video maksimal 10 MB"
        })
    } else {
    return res.status(500).json({
        status: false,
        message: err.message
    })
}
})

module.exports = index;