const express = require('express');
const router = express.Router();
const user = require('./user');
const histori  = require('./histori');
const biodata = require('./biodata');
const admin = require('./admin');

router.use('/user', user);
router.use('/histori', histori);
router.use('/biodata', biodata);
router.use('/admin', admin);

module.exports = router;