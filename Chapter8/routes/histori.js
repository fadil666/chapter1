const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');
const roles = require('../utils/roles')

router.post('/akun/isiGame', middle(roles.User), cont.histori.isiGame);
router.get('/akun/tampilGameSaya', middle(roles.User), cont.histori.tampilGameSaya);
router.patch('/akun/ganti-game/:gameId', middle(roles.User), cont.histori.updateIsiGame);
router.delete('/akun/hapusGame/:gameId', middle(roles.User), cont.histori.hapusGame);
router.get('/tampilSeluruhGame', cont.histori.tampilSeluruhGameUser);

module.exports = router;