const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');
const roles = require('../utils/roles')
const storage = require('../utils/storage')

const multer = require('multer');
const upload = multer();

router.post('/akun/daftaradmin', cont.admin.daftarAdmin);
router.post('/masuk/admin', cont.admin.masukAdmin);
router.get('/akun/admin', middle(roles.Admin), cont.admin.akunAdmin);
router.get('/akun/daftarAdminAll', middle(roles.Admin), cont.admin.daftarAdminAll);
router.delete('/hapusAdmin/:userId', middle(roles.Admin), cont.admin.hapusAdmin);
router.patch('/ganti-password-admin', middle(roles.Admin), cont.admin.gantiPasswordAdmin);
router.get('/akun/detilAdmin/:userId', middle(roles.Admin), cont.admin.detilAdmin);

router.post('/upload/image', middle(roles.Admin), upload.single('image'), cont.admin.upload);

router.post('/upload/video', middle(roles.Admin), upload.single('video'), cont.admin.uploadVideo);

router.post('/upload/single/image',middle(roles.Admin), storage.image.single('media'), (req, res) => {
    const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + req.file.filename;

    return res.json({
        url_file: imageUrl,
        nama_file: req.file.filename
    });
});

router.post('/upload/multi/image', middle(roles.Admin), storage.image.array('media'), (req, res, next) => {
    
    const files = [];
    req.files.forEach(file => {
        const imageUrl = req.protocol + '://' + req.get('host') + '/images/' + req.file.filename;

        files.push(imageUrl);
    });

    return res.json(files)
});

router.post('/upload/single/video',middle(roles.Admin), storage.video.single('media'), (req, res) => {
    const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + req.file.filename;

    return res.json({
        url_file: videoUrl,
        nama_file: req.file.filename
    });
});

router.post('/upload/multi/video', middle(roles.Admin), storage.video.array('media'), (req, res, next) => {
    
    const files = [];
    req.files.forEach(file => {
        const videoUrl = req.protocol + '://' + req.get('host') + '/images/' + req.file.filename;

        files.push(videoUrl);
    });

    return res.json(files)
});

module.exports = router;