const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');
const roles = require('../utils/roles')

router.post('/akun/isibiodata', middle(roles.User), cont.biodata.isiBiodata);
router.delete('/akun/hapusBiodata', middle(roles.User), cont.biodata.hapusBiodata);
router.get('/akun/tampilBiodata', middle(roles.User), cont.biodata.tampilBiodata);
router.get('/tampilBiodataAll', cont.biodata.tampilSeluruhBiodata);
router.patch('/akun/ganti-biodata', middle(roles.User), cont.biodata.updateBiodata);

module.exports = router;