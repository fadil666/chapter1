const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');
const roles = require('../utils/roles')

router.get('/daftar', cont.user.daftarPage);
router.post('/daftar', cont.user.daftar);

router.get('/masuk', cont.user.masukPage);
router.post('/masuk', cont.user.masuk);

router.get('/pesan', cont.user.indexPage);
router.post('/pesan', cont.user.index);

router.get('/lupa-password', cont.user.lupaPasswordPage);
router.post('/lupa-password', cont.user.lupaPassword);

router.get('/reset-password', cont.user.resetPasswordPage);
router.post('/reset-password', cont.user.resetPassword);

router.get('/akun', middle(roles.User), cont.user.akunSaya);
router.get('/user',  cont.user.daftarUser);
router.delete('/hapus/:userId', cont.user.hapususer);
router.patch('/ganti-password', middle(roles.User), cont.user.gantiPassword);
router.get('/detil/:userId', cont.user.detilUser);
router.get('/getAllImages', cont.user.getAllImages);
router.get('/getAllVideos', cont.user.getAllVideos);
router.get('/getImages/:imageId', cont.user.getImages);
router.get('/getVideos/:videoId', cont.user.getVideos);

//oauth google dan facebook
router.get('/login/google', cont.user.google);
router.get('/login/facebook', cont.user.facebook);


module.exports = router;