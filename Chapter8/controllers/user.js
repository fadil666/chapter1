require('dotenv').config();
const { User_Game, User_Biodata, User_History, Image, Video, Tiket } = require('../models');
const { Op } = require("sequelize");
const bcrypt = require('bcrypt');
const jsonToken = require('jsonwebtoken');
const roles = require('../utils/roles')
const usertypes = require('../utils/usertype');
const googleoauth = require('../utils/oauthgoogle');
const facebookoauth = require('../utils/oauthfacebook');
const nodemailer = require('nodemailer');
const webpush = require('web-push');
const { google } = require('googleapis');
const ejs = require('ejs')

const {
    GOOGLE_REFRESH_TOKEN1,
    GOOGLE_ACCESS_TOKEN1,
    GOOGLE_CLIENT_ID1,
    GOOGLE_CLIENT_SECRET1,
    GOOGLE_REDIRECT_URI1,
    VAPID_PUBLIC_KEY,
    VAPID_PRIVATE_KEY,
    VAPID_SUBJECT,
    JWT_TOKEN
} = process.env;

const oauth2Client = new google.auth.OAuth2(
    GOOGLE_CLIENT_ID1,
    GOOGLE_CLIENT_SECRET1,
    GOOGLE_REDIRECT_URI1
);

oauth2Client.setCredentials({
    refresh_token: GOOGLE_REFRESH_TOKEN1
});

function sendEmail(to, subject, html) {

    return new Promise(async (resolve, reject) => {

        try{ 
        const accessToken = await oauth2Client.getAccessToken();
    
        const transport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: 'achmadfadilla112@gmail.com',
                clientId: GOOGLE_CLIENT_ID1,
                clientSecret: GOOGLE_CLIENT_SECRET1,
                refreshToken: GOOGLE_REFRESH_TOKEN1,
                accessToken: accessToken
            }
        })
    
        const mailOptions = {
            to,
            subject,
            html
        }
    
        const response = transport.sendMail(mailOptions)
        resolve(response)
    
        }catch (err){
        reject(err)
        }
    })

}

async function getHtml(filename, data) {
    return new Promise((resolve, reject) => {
        const path = __dirname + '/../views/' + filename;

        ejs.renderFile(path, data, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

module.exports = {
    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            if (!code) {
                const url = googleoauth.generateAuthUrl();
                return res.redirect(url);
            }

            await googleoauth.setCredentials(code);

            const { data } = await googleoauth.getDataUser();

            let exist = await User_Game.findOne({
                where: {
                    email: data.email
                }
            })

            if (!exist) {
                exist = await User_Game.create({
                    username: data.name,
                    email: data.email,
                    role: roles.User,
                    usertype: usertypes.Google
                })
            }

            const payload = {
                id: exist.id,
                username: exist.username,
                email: exist.email,
                role: exist.role,
                usertype: exist.usertype
            }

            const token1 = jsonToken.sign(payload, JWT_TOKEN);

            return res.status(200).json({
                status: true,
                message: 'berhasil',
                data: {
                    id: exist.id,
                    username: exist.username,
                    email: exist.email,
                    token: token1,
                    code: code
                }
            })
        }catch (err){
            next(err);
        }
    },

    facebook: async (req, res, next) => {
        try {
        const code = req.query.code;

            if (!code) {
                const url = facebookoauth.generateAuthURL();
                return res.redirect(url);
            }

            const access_token = await facebookoauth.getAccessToken(code);

            const userInfo = await facebookoauth.getUserInfo(access_token);

            let userExist = await User_Game.findOne({ where: { email: userInfo.email } });

            if (!userExist) {
                userExist = await User_Game.create({
                    username: [userInfo.first_name, userInfo.last_name].join(' '),
                    email: userInfo.email,
                    role: roles.User,
                    usertype: usertypes.Facebook
                });
            }

            const payload = {
                id: userExist.id,
                username: userExist.username,
                email: userExist.email,
                role: userExist.role,
                usertype: userExist.usertype
            };
            const token = jsonToken.sign(payload, JWT_TOKEN);

            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    id: userExist.id,
                    username: userExist.username,
                    email: userExist.email,
                    role: userExist.role,
                    usertype: userExist.usertype,
                    token: token,
                    code: access_token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    daftarPage: (req, res) => {
        res.render('auth/register', { error: null })
    },

    masukPage: (req, res) => {
        res.render('auth/login', { error: null })
    },

    indexPage: (req, res) => {
        res.render('index', { error: null })
    },

    lupaPasswordPage: (req, res) => {
        res.render('auth/forgot-password', { message: null })
    },

    resetPasswordPage: (req, res) => {
        const { token } = req.query;
        res.render('auth/reset-password', { message: null, token })
    },

    daftar: async (req, res, next) => {
        
        try {

            const subscriptions = require('../subscriptions.json');

            const { username, email, password, role = roles.User, usertype = usertypes.Basic } = req.body;

            const sudahAda = await User_Game.findOne({ where: { email: email } });
            if (sudahAda) return res.render('auth/register', { error: 'email sudah terpakai!!!' })
            

            const encr = await bcrypt.hash(password, 10);
            const regis = await User_Game.create({
                username,
                email,
                password: encr,
                role,
                usertype
            })

            const html1 = await getHtml('helo.ejs', { 
                user: { 
                    username: regis.username,
                 } 
                });

            // const response = await sendEmail(`${exist.email}`, 'reset password', `<h1>http://localhost:3000/auth/reset_password?token=${token}</h1>`)
            const response = await sendEmail(`${regis.email}`, 'Selamat datang', `${html1}`)

            const payload = JSON.stringify({
                title: `${regis.username}, selamat akun anda berhasil dibuat`,
                body: 'silahkan cek email untuk pemberitahuan',
            });
        
        // user_id : user.id
        // data: JSON.stringify(subscription);
    
            subscriptions.forEach(subscription => {
                webpush.sendNotification(subscription, payload)
                    .then(result => console.log(result))
                    .catch(e => console.log(e.stack));
            });

            return res.render('auth/login', { error: null });

        }catch (err) {
            next(err);
        }
    },

    index: async (req, res, next) => {
        
        try {

            const subscriptions = require('../subscriptions.json');

            const { username, email, user_id = 1, asal, tujuan, berangkat, kereta,  bayar = 0 } = req.body;

            const regis = await Tiket.create({
                user_id,
                username,
                email,
                asal, 
                tujuan, 
                berangkat, 
                kereta,  
                bayar
            })

            const html1 = await getHtml('pesan.ejs', { 
                user: { 
                    username: regis.username,
                 } 
                });

            // const response = await sendEmail(`${exist.email}`, 'reset password', `<h1>http://localhost:3000/auth/reset_password?token=${token}</h1>`)
            const response = await sendEmail(`${regis.email}`, 'Selamat datang', `${html1}`)


            const payload = JSON.stringify({
                title: `${regis.username}, tiket berhasil di pesan`,
                body: 'silahkan cek email untuk pemberitahuan',
            });
        
        // user_id : user.id
        // data: JSON.stringify(subscription);
    
            subscriptions.forEach(subscription => {
                webpush.sendNotification(subscription, payload)
                    .then(result => console.log(result))
                    .catch(e => console.log(e.stack));
            });

            return res.render('index', { error: null });

        }catch (err) {
            next(err);
        }
    },

    masuk: async (req, res, next) => {
        try {
            const user = await User_Game.authenticate(req.body);
            const accesstoken = user.generateToken();

           return res.render('index', {message: accesstoken})
        } catch (err) {
            next(err);
        }
    },

    akunSaya: (req, res, next) => {
        const user = req.user;
        
        try {
            const date = new Date();
            const formatDate = date.toLocaleDateString('id');
            var curr_hour = date.getHours();
		    var curr_minute = date.getMinutes();
		    var curr_second = date.getSeconds();

            const jam = curr_hour + ':' + curr_minute + ':' + curr_second;

            return res.status(200).json({
                status: true,
                message: 'autentifikasi berhasil',
                data: {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    role: user.role,
                    usertype: user.usertype,
                    date: formatDate,
                    time: jam
                }
            });
            
        }catch (err) {
            next(err);
        }
        
    },


    gantiPassword: async (req, res, next) => {

        try {
            const { 
                 passwordLama,
                 passwordBaru,
                 passwordBaru2
            } = req.body;

            const usercompare = await User_Game.findOne({ 
                where: { 
                    id: req.user.id
                }});
            if (!usercompare) {
                return res.status(400).json({
                    status: false,
                    message: 'user tidak di temukan!'
                })
            }
    
            const pass = await bcrypt.compare(passwordLama, usercompare.password);
            if (!pass) {
                return res.status(400).json({
                    status: false,
                    message: 'password salah!!'
                })
            }

            if (passwordBaru !== passwordBaru2) 
            return res.status(422).json({
                status: false,
                message: 'password 1 dan password 2 tidak sama!'
            });

            const hashedPassword = await bcrypt.hash(passwordBaru, 10);
             await usercompare.update({password: hashedPassword});

            return res.status(200).json({
                success: true,
                message: 'Password berhasil di ubah'
            });
        } catch (err) {
            res.status(500).json({
                status: false, 
                message: err.message
            });
        }
    },

    hapususer: async (req, res, next) => {
        try {
        const { userId } = req.params;
        // ini akan menghapus user, histori game, dan biodata berdasarkan id
        const user = await User_Game.findOne({ 
            where: { 
                [Op.and]: [
                    { id: userId },
                    { role: roles.User }
                  ]
            } 
        });
            if (!user) return res.status(404).json({ 
                status: false, 
                message: `User dengan id ${userId} tidak di temukan!` 
            });
        
            await User_Game.destroy({ where: { id: userId }});
            await User_Biodata.destroy({where: { id_user : userId}});
            await User_History.destroy({where: { id_user : userId}});

        return res.status(200).json({
            status: true,
            message: 'data user telah berhasil di hapus',
            data: user
        })
    } catch (err) {
        next(err);
    }

    },

    daftarUser: async  (req, res, next) => {
        
        try { 
           const users = await User_Game.findAll({
            where: {
                role: roles.User
            }
           });

        return res.status(200).json({
            status: true,
            message: 'berhasil dapat data',
            data: users
        })
    }catch (err) {
        next(err);
    }  
    },

    detilUser: async (req, res, next) => {
        try {
            const { userId } = req.params;

            const user = await User_Game.findOne({ 
                where: { 
                    [Op.and]: [
                        { id: userId },
                        { role: roles.User }
                      ]
                    }
            });
                
                if (!user) return res.status(404).json({ 
                    status: false, 
                    message: `User dengan id ${userId} tidak ditemukan` 
                });
    
            return res.status(200).json({
                status: true,
                message: 'data berhasil ditemukan',
                data: user
            })
        } catch (err) {
            next(err);
        }
    },

    getAllImages: async (req, res, next) => {
        try{

            const images = await Image.findAll();

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: images
            })

        }catch (err){
            next(err);
        }
    },

    getAllVideos: async (req, res, next) => {
        try{

            const videos = await Video.findAll();

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: videos
            })

        }catch (err){
            next(err);
        }
    },

    getImages: async (req, res, next) => {
        try{
            const { imageId } = req.params;
            const images = await Image.findOne({
                where: {
                    id: imageId
                }
            });

            if (!images) {
                return res.status(404).json({
                    status: false,
                    message: `gambar dengan ${imageId} tidak ditemukan`,
                    data: null
                })
            }

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: images
            })

        }catch (err){
            next(err);
        }
    },

    getVideos: async (req, res, next) => {
        try{
            const { videoId } = req.params;
            const videos = await Video.findOne({
                where: {
                    id: videoId
                }
            });

            if (!videos) {
                return res.status(404).json({
                    status: false,
                    message: `video dengan ${videoId} tidak ditemukan`,
                    data: null
                })
            }

            return res.status(200).json({
                status: true,
                message: 'berhasil dapat data',
                data: videos
            })

        }catch (err){
            next(err);
        }
    },

    lupaPassword: async (req, res, next) => {
        try {
            const { email } = req.body;

            const user = await User_Game.findOne({
                where: {
                    email
                }
            })

            if (user) {
                const payload = { user_id: user.id }
                const token = jsonToken.sign(payload, JWT_TOKEN)
                const link = `http://localhost:3000/user/reset-password?token=${token}`

                const html1 = await getHtml('email/reset-password.ejs', { 
                    
                        username: user.username,
                        link: link
                     
                    });
    
                // const response = await sendEmail(`${exist.email}`, 'reset password', `<h1>http://localhost:3000/auth/reset_password?token=${token}</h1>`)
                const response = await sendEmail(user.email, 'Lupa Password', html1)
            }

            return res.render('auth/forgot-password', { message: 'we will send email for reset password if the email is exist on our database!' });
        }catch (err){
            next(err);
        }
    },

    resetPassword: async (req, res, next) => {
        try {
            const subscriptions = require('../subscriptions.json');
            const { token } = req.query;
            const { passwordBaru, passwordBaru2 } = req.body;

            if (!token) return res.render('auth/reset-password', { message: 'invalid token', token })
            if (passwordBaru != passwordBaru2) return res.render('auth/reset-password', { message: 'password 1 dan 2 tidak sama' })

            const payload = jsonToken.verify(token, JWT_TOKEN)

            const ecr = await bcrypt.hash(passwordBaru, 10)

            const user = await User_Game.update({
                password: ecr
            }, {
                where: {
                    id: payload.user_id
                }
            })

            const payload1 = JSON.stringify({
                title: `password berhasil di ubah`,
                body: 'silahkan login',
            });
        
        // user_id : user.id
        // data: JSON.stringify(subscription);
    
            subscriptions.forEach(subscription => {
                webpush.sendNotification(subscription, payload1)
                    .then(result => console.log(result))
                    .catch(e => console.log(e.stack));
            });

            return res.render('auth/login', { error : null })
        }catch (err){
            next(err);
        }
    }
}