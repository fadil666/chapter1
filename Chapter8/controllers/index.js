const user = require('./user')
const biodata = require('./biodata')
const histori = require('./histori')
const admin = require('./admin')

module.exports = { user, biodata, histori, admin};