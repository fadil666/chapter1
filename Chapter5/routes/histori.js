const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');

router.post('/akun/isiGame', middle.masukDulu, cont.auth.isiGame);
router.get('/akun/tampilGameSaya', middle.masukDulu, cont.auth.tampilGameSaya);
router.patch('/akun/ganti-game/:gameId', middle.masukDulu, cont.auth.updateIsiGame);
router.delete('/akun/hapusGame/:gameId', middle.masukDulu, cont.auth.hapusGame);
router.get('/tampilSeluruhGame', cont.auth.tampilSeluruhGameUser);

module.exports = router;