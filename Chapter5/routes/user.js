const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');


router.post('/daftar', cont.auth.daftar);
router.post('/masuk', cont.auth.masuk);
router.get('/akun', middle.masukDulu, cont.auth.akunSaya);
router.get('/user', cont.auth.daftarUser);
router.delete('/hapus/:userId', cont.auth.hapusUser);
router.patch('/ganti-password', middle.masukDulu, cont.auth.gantiPassword);
router.get('/detil/:userId', cont.auth.detilUser);

module.exports = router;