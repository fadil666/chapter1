const express = require('express');
const morgan = require('morgan');
const router = require('./routes');
require('dotenv').config();
const app = express();



const {
    http_port = 3000
} = process.env;

app.use(express.json());
app.use(morgan('dev'));
app.use(router);

app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'hayo tersesat ya?'
    })
})

app.use((err, req, res, next) => {
    console.log(err);
    return res.status(500).json({
        status: false,
        message: err.message
    })
})

app.listen(http_port, () => console.log("listening on port", http_port));